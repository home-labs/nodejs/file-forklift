export class FileURL {

    static get protocolPattern() {
        return /file:\/{2}(.+)(?=:[0-9]+:[0-9]+)/;
    }

}
