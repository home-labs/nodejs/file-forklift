import NodeURL from 'node:url';

import { Helpers } from './helpers/namespace.mjs';


export class FileProtocolImportTracker {

    #_errorStack!: string[];

    #_callerUrlname!: string;

    get callerUrlname(): string {
        return this.#_callerUrlname;
    }

    get errorStack(): string[] {
        return this.#_errorStack;
    }

    constructor() {
        this.#resolveUrlname();
    }

    #resolveUrlname(): void {

        const error = new Error();

        let compatibility: RegExpMatchArray | null = null;

        let result: string | undefined;

        this.#_errorStack = error.stack?.split('\n') as string[];

        for (let item of [...this.#_errorStack].reverse()) {

            compatibility = item.match(Helpers.FileURL.protocolPattern);

            if (compatibility) {
                break;
            }
        }

        if (compatibility) {
            result = compatibility[1];
            this.#_callerUrlname = NodeURL.fileURLToPath(`file:/${result}`);
        }

    }

}
