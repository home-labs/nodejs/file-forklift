import { FileProtocolImportTracker } from '../../../../index.mjs';


export class Test4 {

    constructor() {
        this.#initialize();
    }

    #initialize() {

        const fileProtocolTracker = new FileProtocolImportTracker();

        const fileCallerPath = fileProtocolTracker.callerUrlname;

        console.log(`\n------------`);
        console.log(`Absolute current file pathname`, import.meta.url)
        console.log(`\nFile pathname that called the absolute current file pathname: `, fileCallerPath, ` (THE GOAL).`);
        console.log(`\nerror stack (displayed by test4 file): `, fileProtocolTracker.errorStack);
    }

}
